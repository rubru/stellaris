library(tidyverse)
library(ggplot2)
library(lubridate)

data <- read_csv("rotai.csv")

data <- data %>%
rename(ingame_years = ingame)

data <- data %>%
  mutate(oog_mins = make_difftime(data$oog, units = "mins")-554) %>%
  mutate(ingame_years = ingame_years-2216)

ggplot(data, aes(oog_mins, ingame_years)) +
  geom_line() +
  geom_point() +
  geom_smooth(method = "lm") +
  scale_x_continuous(minor_breaks = seq(0, 220, 10)) +
  theme_dark()
